FROM ghdl/ghdl:fedora36-gcc-11.3.0

COPY riscv_toolchain /opt/toolchain
ENV PATH="${PATH}:/opt/toolchain/bin"

RUN python3 -m ensurepip --upgrade 
RUN pip3 install --no-cache-dir --upgrade pip && \
    pip3 install cocotb
RUN pip3 install cocotbext-wishbone cocotbext-spi cocotbext-uart
RUN pip3 install pyuvm
RUN pip3 install numpy
RUN pip3 install pytest

# CMD ["bash"]

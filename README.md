# GHDL and RISC-V toolchain

This docker image provides the GHDL 3.0, cocotb, pyuvm, and RISC-V toolchain.

#### RISC-V toolchain specification:
* GCC - 12.1.0	
* binutils - 2.39	
* march - multilib: rv32i rv32ic rv32im rv32imc	
* mabi - ilp32	
* c-lib - newlib

Credit: https://github.com/stnolting/riscv-gcc-prebuilt

#### How to compile

The Docker installation is required

```
docker build -t ghdl_riscv .
```

#### Launch example
If the current directory contains the FPGA project files:
```
docker run -it -v $(pwd):/work ghdl_riscv
```
This command will mount the current HOST directory into /word and launch the container.
